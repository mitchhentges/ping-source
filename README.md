# ping-source

Generates `PING` data for [messageboard.fuzzlesoft.ca](http://messageboard.fuzzlesoft.ca)

## Usage

`./ping-source DEV_MACHINE MINUTES_BETWEEN_PINGS` or `cargo run DEV_MACHINE MINUTES_BETWEEN_PINGS`
