#![feature(proc_macro)]

#[macro_use] extern crate serde_derive;

extern crate chrono;
extern crate hyper;
extern crate serde;
extern crate serde_json;
extern crate ws;

use std::env;
use std::{thread, time};
use std::sync::{Arc, Mutex};
use ws::{Handler, Sender, Handshake, Result, Message};

struct Client {
    out: Sender,
    sender: Arc<Mutex<Option<Sender>>>
}

impl Handler for Client {
    fn on_open(&mut self, _: Handshake) -> Result<()> {
        *self.sender.lock().unwrap() = Some(self.out.clone());
        Ok(())
    }

    fn on_message(&mut self, _: Message) -> Result<()> {
        Ok(())
    }
}

fn main() {
    let sender = Arc::new(Mutex::new(Option::None::<Sender>));
    {
        let sender = sender.clone();
        thread::spawn(move || {
            loop {
                let sender = sender.clone();
                thread::spawn(move || {
                    println!("Connecting to messageboard.fuzzlesoft.ca:9011");
                    ws::connect("ws://messageboard.fuzzlesoft.ca:9011", |out| {
                        Client { out: out, sender: sender.clone() }
                    }).unwrap();
                }).join().unwrap();
            }
        });
    }

    if let (Some(device_name), Some::<u64>(minutes)) = (env::args().nth(1), env::args().nth(2).and_then(|str| str.parse().ok())) {
        loop {
            let client = hyper::client::Client::new();
            let before = time::Instant::now();
            if client.get("https://spyu.ca/api/ping-source").send().is_err() {
                println!("Failed to request to Spyu");
            }
            let elapsed_nanos = before.elapsed().subsec_nanos();

            let event = Event {
                _type: "PING".to_owned(),
                date: chrono::UTC::now().to_rfc3339(),
                value: elapsed_nanos as f64,
                device: device_name.clone()
            };

            if let Ok(sender) = sender.lock() {
                if let Some(ref sender) = *sender {
                    if sender.send(serde_json::to_string(&event).unwrap()).is_err() {
                        println!("Failed to send ws message");
                    }
                }
            }

            thread::sleep(time::Duration::from_secs(60 * minutes));
        }
    } else {
        println!("Usage: ./ping-source DEVICE_NAME TIME_BETWEEN_SNAPSHOTS");
    }
}

#[derive(Serialize)]
struct Event {
    _type: String,
    date: String,
    value: f64,
    device: String
}